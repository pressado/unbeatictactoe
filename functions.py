print('\nBem-vindo ao jogo da velha imbatível. Você não vai vencer.')

board = {
    1: ' ', 2: ' ', 3: ' ',
    4: ' ', 5: ' ', 6: ' ',
    7: ' ', 8: ' ', 9: ' '
        }

def print_board(board):
    print(board[1] + ' | ' + board[2] + ' | ' + board[3] + '\n' +
          '--+---+--' + '\n' +
          board[4] + ' | ' + board[5] + ' | ' + board[6] + '\n' +
          '--+---+--' + '\n' +
          board[7] + ' | ' + board[8] + ' | ' + board[9])  


def define_player():
    computer = 'O'
    player = input('\nCom qual letra você quer Jogar?').upper()
    if len(player) == 0:
        player = 'X'
    else:
        player = player[0]
    if player == 'O':
        computer = 'X'
    print(f'Você joga com {player}! Computador joga com {computer}!\n')
    return player, computer
        
player, computer = define_player()

def check_freespace(position):
    if board[position] == ' ':
        return True
    else:
        return False


def insert_xo(xo, position):
    if check_freespace(position):
        board[position] = xo
        print_board(board)
        if check_tie():
            print('Empate!')
            exit()
        if check_win():
            if xo == computer:
                print(f'Jogador {computer} venceu!')
                exit()
            else:
                print(f'Jogador {player} venceu!')
                exit()
        return
    else:
        position = int(input('Posição ocupada! Tente novamente: '))
        insert_xo(xo, position)
        return


def check_win():
    if (board[1] == board[2] and board[1] == board[3] and board[1] != ' '):
        return True
    elif (board[4] == board[5] and board[4] == board[6] and board[4] != ' '):
        return True
    elif (board[7] == board[8] and board[7] == board[9] and board[7] != ' '):
        return True
    elif (board[1] == board[4] and board[1] == board[7] and board[1] != ' '):
        return True
    elif (board[2] == board[5] and board[2] == board[8] and board[2] != ' '):
        return True
    elif (board[3] == board[6] and board[3] == board[9] and board[3] != ' '):
        return True
    elif (board[1] == board[5] and board[1] == board[9] and board[1] != ' '):
        return True
    elif (board[7] == board[5] and board[7] == board[3] and board[7] != ' '):
        return True
    else:
        return False


def check_which_mark_won(mark):
    if (board[1] == board[2] and board[1] == board[3] and board[1] == mark):
        return True
    elif (board[4] == board[5] and board[4] == board[6] and board[4] == mark):
        return True
    elif (board[7] == board[8] and board[7] == board[9] and board[7] == mark):
        return True
    elif (board[1] == board[4] and board[1] == board[7] and board[1] == mark):
        return True
    elif (board[2] == board[5] and board[2] == board[8] and board[2] == mark):
        return True
    elif (board[3] == board[6] and board[3] == board[9] and board[3] == mark):
        return True
    elif (board[1] == board[5] and board[1] == board[9] and board[1] == mark):
        return True
    elif (board[7] == board[5] and board[7] == board[3] and board[7] == mark):
        return True
    else:
        return False


def check_tie():
    for k in board.keys():
        if board[k] == ' ':
            return False
    else:
        return True


def player_move():
    position = input(f'Jogador {player}, selecione o número da casa: ')
    try:
        position = int(position)
        if position < 1 or position > 9:
            print('Selecione um número válido, entre 1 e 9.')
            player_move()
        else:
            insert_xo(player, position)
            return
    except ValueError:
        print('Apenas números!')
        player_move()
    

def computer_move():
    best_score = -800 #não importa o valor, só precisa ser bem pequeno
    best_move = 0

    for k in board.keys():
        if board[k] == ' ':
            board[k] = computer
            score = minimax(board, False)
            board[k] = ' '
            if score > best_score:
                best_score = score
                best_move = k
    insert_xo(computer, best_move)
    return


def minimax(board, is_maximizing):
    if check_which_mark_won(computer):
        return 1
    elif check_which_mark_won(player):
        return -1
    elif check_tie():
        return 0

    if is_maximizing:
        best_score = -800
        for k in board.keys():
            if board[k] == ' ':
                board[k] = computer
                score = minimax(board, False)
                board[k] = ' '
                if score > best_score:
                    best_score = score
        return best_score
    else:
        best_score = 800
        for k in board.keys():
            if board[k] == ' ':
                board[k] = player
                score = minimax(board, True)
                board[k] = ' '
                if score < best_score:
                    best_score = score
        return best_score